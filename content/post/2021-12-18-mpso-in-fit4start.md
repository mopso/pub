---
title: Mopso selected at Fit4Start
date: 2021-12-18
description: Mopso will have access to MeluXina supercomputer
---

We are pleased to inform you that Mopso has been selected to the [12th edition of Fit 4 Start](https://www.startupluxembourg.com/fit-4-start-edition-12-selected-startups).

Mopso will soon open a branch in Luxembourg and will have access to the [MeluXina supercomputer](https://docs.lxp.lu/system/overview/).

MeluXina is designed as a modular system, providing facilities for HPC, HPDA and AI workloads. The following architecture diagram gives an overview of the different modules and how they are interconnected.

![MeluXina architecture](https://docs.lxp.lu/system/2021-02-09-meluxina_system_architecture.svg)


