---
title: Mopso passed EssifLab review
date: 2022-04-13
description: Mopso Amlel admited to the third stage of EssifLab
---
The Amlet project was admited to the third level of EssifLab project.

eSSIF-Lab is an EU-funded project and aims at advancing the broad uptake of Self-Sovereign Identities (SSI) as a next generation, open and trusted digital identity solution for faster and safer electronic transactions via the Internet and in real life.

More info https://essif-lab.eu/meet-the-essif-lab-ecosystem-the-2nd-business-oriented-programme-participants/