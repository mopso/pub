---
title: "Amlet selected by B-hub for Europe"
description: "We have been selected among startups active in the blockchain sector by B-hub for Europe to explore" 
date: 2021-05-03
---
And here we go 🚀 We have been selected among startups active in the blockchain sector by B-hub for Europe to explore collaboration possibilities

This is amazing as we can discuss with our peers from all over Europe and learn a lot from the blockchain market 💻

👉 Check it out on [B-Hub-for-Europe website](https://bit.ly/3gFfybj)

👉 Learn about us on our website: https://amlet.eu/

