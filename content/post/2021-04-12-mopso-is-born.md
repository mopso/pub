---
title: Mopso is born
date: 2021-04-12
description:  Mopso startup is now one of most promising startup in Europe
---

After a year+ of incubation in LinkedData.Center, Mopso is born.
Mopso is one of the most innovative European Regtech in the panorama of Anti-Money-Laundering and Self-Sovereign Identity for Customer Due Diligence.

#AML #SSI #CDD #banking #fintech #regtech #risk #compliance #blockchain

more info https://mopso.eu/ https://amlet.eu/
