---
title: New pamla check
date: 2022-10-25
description: The new Personal AML Alert check is born
---


The new [Personal AML Alert (PAMLA)](https://developer.mopso.io/docs/reference/agency/pamla/) check check was added to the [Agency](https://developer.mopso.io/docs/reference/agency/) features
