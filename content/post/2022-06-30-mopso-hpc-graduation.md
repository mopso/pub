---
title: Mopso wins Fit4Start "HPC" category
date: 2022-06-30
description: Luxinnovation graduated Mopso in the HPC and data analysis category.
---
Luxinnovation graduated the best start-ups from its 12th Fit4Start on the ICT Spring stage, including Mopso in the new _HPC and data analysis_ category.

"This new promotion of graduate start-ups is in line with the previous ones in terms of quality", comments the CEO of Luxinnovation, Sasha Baillie. The additional contribution of the HPC dimension and the enthusiasm of the candidates have also shown how this type of support is very interesting for start-ups. More than ever, they are key players in the development of our national data-centric ecosystem.

More on https://paperjam.lu/article/mopso-et-rss-hydro-premiers-la

