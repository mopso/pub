---
title: mopso selected in MPS innovation initiative
date: 2020-02-04
description: mopso has been selected for the OfficinaMPS open banking initiative
---


mopso has been selected for the [OfficinaMPS](https://www.officina.mps.it/news/officinamps-8-startup-finaliste-per-l-iniziativa-dedicata-all-open-banking.html) open
 banking initiative, 
the permanent laboratory of Banca Monte dei Paschi di Siena dedicated to innovation.

