---
title: Mopso logo usage guidelines
date: 2022-09-28T17:55:00.336Z
description: The new collection of Mopso logo
---

The usage of the mopso logo must be authorized.

**Reference logo**: https://pub.mopso.io/img/logo/v2/mopso-white.png

![Mopso logo](https://pub.mopso.io/img/logo/v2/mopso-white.png "https://pub.mopso.io/img/logo/v2/mopso-white.png")

**Other logo variations**:
* https://pub.mopso.io/img/logo/v2/mopso-just-text-transparent.png
* https://pub.mopso.io/img/logo/v2/mopso-just-text.svg
* https://pub.mopso.io/img/logo/v2/mopso-no-text-transparent.png
* https://pub.mopso.io/img/logo/v2/mopso-no-text.svg
* https://pub.mopso.io/img/logo/v2/mopso-svg.svg
* https://pub.mopso.io/img/logo/v2/mopso-transparent.png