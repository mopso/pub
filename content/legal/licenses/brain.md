---
title: The Mopso Brain License
date: 2021-07-18
description: The Mopso Brain License agreement
---
For Mopso Brain license please contact sales representative.

Mopso Brain includes following great pieces of open source software:

- [SDAAS-ce](https://github.com/linkeddatacenter/sdaas-ce) by LinkedData.Center
- [SDAAS-rdfstore](https://github.com/linkeddatacenter/sdaas-rdfstore) by LinkedData.Center
- [BOTK-core](https://github.com/linkeddatacenter/BOTK-core) by LinkedData.Center
- [BOTK-amlo](https://github.com/linkeddatacenter/BOTK-amlo) by LinkedData.Center
- [GRLC](https://github.com/CLARIAH/grlc) by Albert Meroño, VU University Amsterdam
- [Blazegraph database](https://github.com/blazegraph/database) by Free Software Foundation, Inc

It is based on thes well known efforts:
- Apache http
- PHP
- Node.js
- Java
- jetty
- docker
- docker-compose
- kubernetes
- git
- gitlab
- linux


See specific sw agreement for more info about licenses and authors
